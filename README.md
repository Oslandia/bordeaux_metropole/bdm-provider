# Provider Bordeaux Métropole - QGIS Plugin

[![pipeline status](https://gitlab.com/Oslandia/bordeaux_metropole/bdm-metadata/badges/main/pipeline.svg)](https://gitlab.com/Oslandia/bordeaux_metropole/bdm-metadata/-/commits/main)  
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)


[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

## Generated options

### Plugin

"plugin_name": Provider Bordeaux Métropole
"plugin_name_slug": bdm_provider
"plugin_name_class": BdmProvider

"plugin_category": Database
"plugin_description_short": Plugin permettant d'ajouter des données issues des bases de données de Bordeaux Métropole en fonction de son profil d'habilitation
"plugin_description_long": Plugin permettant d'ajouter des données issues des bases de données de Bordeaux Métropole en fonction de son profil d'habilitation
"plugin_description_short": Plugin permettant d'ajouter des données issues des bases de données de Bordeaux Métropole en fonction de son profil d'habilitation
"plugin_icon": default_icon.png

"author_name": Benoit DUCAROUGE
"author_email": benoit.ducarouge@oslandia.com

"qgis_version_min": 3.16
"qgis_version_max": 3.99

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.  
Static code analisis is based on: Flake8

See also: [contribution guidelines](CONTRIBUTING.md).

## CI/CD

Plugin is linted, tested and packaged with GitLab.


### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <None>
- repository: <https://gitlab.com/Oslandia/bordeaux_metropole/bdm-provider>
- tracker: <None>

----

## License

Distributed under the terms of the [`GPLv2+` license](LICENSE).
