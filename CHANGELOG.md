# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).
## 1.2.0 - 2024-01-03

- Ajout d'une classification pour les fonds de plans

## 1.1.1 - 2023-12-14

- Fix sur la gestion de l'ajout des relations

## 1.1.0 - 2023-11-23

- Disposer d'une option permettant de ne pas charger les données en relation
- Ne pas afficher les données SOCL_ dans le catalogue
- Ne plus classer les tables de relation dans un groupe spécifique

## 1.0.0-beta8 - 2021-10-11

- Changement nom des relations N-M pour meilleur intégration des relations "retour"
- Bugfix gestion des alias
- Bugfix chargement des relations déjà chargée en mode alias

## 1.0.0-beta7 - 2021-10-11

- Meilleur gestion des alias et des relations

## 1.0.0-beta6 - 2021-10-11

- Ajout des relations dans les 2 sens
- Ajout du nom dans le provider

## 1.0.0-beta5 - 2021-09-13

- Ajout des alias pour les colonnes
- Ajout des listes pour les attributs
- Ajout des layers dans des groupes en fonction de leur type

## 1.0.0-beta4 - 2021-08-30

- Ajout des relations de graphe

## 1.0.0-beta3 - 2021-08-26

- Gestion de la sauvegarde des relations dans les formulaires d'attributs lors de la création d'un style par defaut
- Correction bugs relation n-m
- Ne recharge pas les tables de relations et les layers en relation si déjà chargés

## 1.0.0-beta2 - 2021-08-23

- Ajout automatique des couches en relations
- Ajout des relations 1-n et n-m au niveau du projet
- Ajout de la traduction fr
- Ne recharge plus les couches déjà chargées
- Réinitialisation de la fenêtre de dialogue à chaque ouverture
- Améliorations ergonomiques (élément sélectionné surligné)
- configuration de l'authentification et de la connexion à partir d'une variable d'environnement

## 1.0.0-beta1 - 2021-08-18

- Ajout du provider BDM basé sur le catalogue WPS
- Chargement des données Oracle
- Chargement des données WMTS
- Récupération des métadonnées
- Centralisation des paramètres dans l'interface de configuration du plugin

## 0.1.0 - 2021-08-10

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
