<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>ProviderForm</name>
    <message>
        <location filename="../../gui/provider_form.ui" line="61"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../../gui/provider_form.ui" line="20"/>
        <source>Metadata :</source>
        <translation>Métadonnées :</translation>
    </message>
    <message>
        <location filename="../../gui/provider_form.ui" line="71"/>
        <source>Backgrounds</source>
        <translation>Fonds de plan</translation>
    </message>
    <message>
        <location filename="../../gui/provider_form.ui" line="27"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="../../gui/provider_form.ui" line="34"/>
        <source>Enable aliases</source>
        <translation>Activer les alias</translation>
    </message>
    <message>
        <location filename="../../gui/provider_form.ui" line="87"/>
        <source>Load relations layers</source>
        <translation>Chargement des relations</translation>
    </message>
</context>
<context>
    <name>dlg_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="97"/>
        <source>Features</source>
        <translation>Fonctionnalités</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="110"/>
        <source>WPS service</source>
        <translation>Service WPS :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="129"/>
        <source>Request URL:</source>
        <translation>Base de l'URL de requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="329"/>
        <source>Authentication Id</source>
        <translation>Id d'authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="143"/>
        <source>Request key</source>
        <translation>Clé de connexion BDM :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="186"/>
        <source>WPS version</source>
        <translation>Version WPS :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="233"/>
        <source>HTTP user-agent:</source>
        <translation>HTTP user-agent :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="343"/>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="308"/>
        <source>Host</source>
        <translation>Serveur :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="315"/>
        <source>Port</source>
        <translation>Port :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="336"/>
        <source>Default SRID</source>
        <translation>SRID par défaut :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="350"/>
        <source>Default Id Column</source>
        <translation>Colonne d'Id par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="492"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="501"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="510"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Activer le mode debug (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="542"/>
        <source>Open documentation</source>
        <translation>Ouvrir la documentation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="545"/>
        <source>Help</source>
        <translation>Aide en ligne</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="552"/>
        <source>Version used to save settings:</source>
        <translation>Version des paramètres :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="571"/>
        <source>Report an issue</source>
        <translation>Signaler une anomalie</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="103"/>
        <source>api_auth_id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="172"/>
        <source>api_wps_service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="179"/>
        <source>Pigma CSW URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="193"/>
        <source>api_bm_pigma_url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="200"/>
        <source>api_bm_pigma_url_csw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="207"/>
        <source>api_wps_version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="214"/>
        <source>api_request_key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="262"/>
        <source>Pigma URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="294"/>
        <source>api_oracle_port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="301"/>
        <source>api_oracle_auth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="322"/>
        <source>api_oracle_database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="357"/>
        <source>api_oracle_default_srid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="364"/>
        <source>api_oracle_id_column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="371"/>
        <source>api_oracle_host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="397"/>
        <source>Namespaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="403"/>
        <source>GML namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="410"/>
        <source>api_ns_ows_uri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="417"/>
        <source>api_ns_wps_uri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="424"/>
        <source>api_ns_gml_uri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="431"/>
        <source>BM namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="438"/>
        <source>OWS namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="445"/>
        <source>WPS namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="452"/>
        <source>api_ns_bm_uri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="459"/>
        <source>Pigma namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="466"/>
        <source>api_ns_bm_pigma_uri</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
