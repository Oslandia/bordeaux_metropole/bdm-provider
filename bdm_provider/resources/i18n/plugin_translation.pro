FORMS = ../../gui/dlg_settings.ui \
    ../../gui/provider_form.ui

SOURCES= ../../plugin_main.py \
    ../../core/layer_model.py \
    ../../core/provider_bdm.py \
    ../../core/provider_dialog.py \
    ../../core/themes.py \
    ../../toolbelt/log_handler.py \
    ../../toolbelt/network_manager.py \
    ../../toolbelt/ows.py \
    ../../toolbelt/preferences.py \
    ../../toolbelt/wmtsclient.py \
    ../../toolbelt/wpsclient.py

TRANSLATIONS = bdm_provider_fr.ts
