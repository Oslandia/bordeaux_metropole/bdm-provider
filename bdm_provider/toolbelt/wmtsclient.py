#########################################################################
#
# Copyright (C) 2016 Bordeaux Métropole
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

# pylint: disable=E0611

"""
Package de gestion des objets WMTS
"""
from xml.dom import minidom

from lxml import etree
from qgis.core import QgsDataSourceUri, QgsProject, QgsRasterLayer
from qgis.PyQt.Qt import QUrl

from bdm_provider.toolbelt.network_manager import NetworkRequestsManager
from bdm_provider.toolbelt.ows import LayerWMTS, ServiceIdentification, TileMatrixSet
from bdm_provider.toolbelt.preferences import PlgOptionsManager


class WMTSExceptionReport(Exception):
    """
    Classe d'exception
    """

    def __init__(self, code, message):
        super(WMTSExceptionReport, self).__init__()
        self.code = code
        self.message = message


class WMTSClientException(Exception):
    def __init__(self, *args, **kwargs):
        super(WMTSClientException, self).__init__(*args, **kwargs)


class WMTSException(Exception):
    """
    Classe d'exception
    """

    def __init__(self, *args, **kwargs):
        super(WMTSException, self).__init__(*args, **kwargs)


class WMTSReader(object):
    """
    Classe d'envoi et de réception de requêtes WMTS
    """

    def __init__(
        self,
        key,
        version=PlgOptionsManager.get_plg_settings().wmts_version,
        service=PlgOptionsManager.get_plg_settings().wmts_service,
    ):
        self._key = key
        self._version = version
        self._service = service

    def _readFromStringXML(self, xml):
        # Parsing xml
        return minidom.parseString(xml)

    def _readFromUrl(self, url, data, method="GET"):
        """
        Envoi de la requête et réception de la réponse
        @param url: Url du service
        @param data: Données de la requête
        @param method: Méthode de la requête (GET, POST, ...)
        @return: Instance minidom de la réponse
        """
        if method == "GET":
            data["key"] = self._key
            data["version"] = self._version
            data["service"] = self._service

            # Requête HTTP
            try:
                opener = NetworkRequestsManager()
                requestUrl = opener.encodeUrl(url, data)
                response = opener.get_url(QUrl(requestUrl))
            except Exception:
                raise WMTSClientException(
                    "Erreur lors de l'ouverture de l'url {requestUrl}".format(
                        requestUrl=requestUrl
                    )
                )

            # Parsing en xml de la réponse
            return minidom.parseString(response)


class WMTSCapabilitiesReader(WMTSReader):
    """
    Classe d'envoi et de réception d'une requête GetCapabilities
    """

    def __init__(
        self,
        key,
        version=PlgOptionsManager.get_plg_settings().wmts_version,
        service=PlgOptionsManager.get_plg_settings().wmts_service,
    ):
        super(WMTSCapabilitiesReader, self).__init__(
            key, version=version, service=service
        )

    def readFromStringXML(self, xml):
        """
        Lecture du xml résultant du getCapabilities
        @param xml: Flux XML
        @type xml: str
        """
        return self._readFromStringXML(xml)

    def readFromUrl(self, url):
        """
        Envoi de la requête et réception de la réponse
        @param url: Url du service
        @return: Instance minidom de la réponse
        """
        return self._readFromUrl(url, {"request": "GetCapabilities"})


class WMTSClient(object):
    """
    Classe de communication avec un service WMTS
    """

    def __init__(
        self,
        url,
        key,
        version=PlgOptionsManager.get_plg_settings().wmts_version,
        service=PlgOptionsManager.get_plg_settings().wmts_service,
    ):
        self.key = key
        self.url = url
        self.version = version
        self.service = service
        self._capabilities = None
        self.identification = None
        self.layers = []
        self.tileMatrixSets = []

    def getCapabilities(self, xml=None):
        """
        Récupère les capacités du service WMTS
        @param xml: Flux XML
        @type xml: str
        """
        reader = WMTSCapabilitiesReader(
            self.key, version=self.version, service=self.service
        )
        if xml is None:
            self._capabilities = reader.readFromUrl(self.url)
        else:
            self._capabilities = reader.readFromStringXML(xml)

        self._parseCapabilities(self._capabilities)

    def _parseCapabilities(self, xml):
        """
        Parse les capacités du service WMTS
        @param xml: Instance minidom des capacités
        """
        elements = [
            x
            for x in xml.documentElement.childNodes
            if x.nodeType == minidom.Node.ELEMENT_NODE
        ]
        for element in elements:
            if element.localName == "ServiceIdentification":
                self.identification = ServiceIdentification(
                    element, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri
                )
            elif element.localName == "Contents":
                for child in element.getElementsByTagName("Layer"):
                    self.layers.append(
                        LayerWMTS(
                            child, ns=PlgOptionsManager.get_plg_settings().ns_ows_uri
                        )
                    )
                for child in element.getElementsByTagName("TileMatrixSet"):
                    if (
                        len(
                            child.getElementsByTagNameNS(
                                PlgOptionsManager.get_plg_settings().ns_ows_uri,
                                "Identifier",
                            )
                        )
                        > 0
                    ):
                        self.tileMatrixSets.append(
                            TileMatrixSet(
                                child,
                                ns=PlgOptionsManager.get_plg_settings().ns_ows_uri,
                            )
                        )


class WMTSMetaData(object):
    """
    Class de gestion des métadonnées
    """

    def __init__(self, uuid):
        self._uuid = uuid
        self._metaData = None

    def getMetaData(self, xsltTemplateFile=None):
        """
        Récupération des metadonnées
        @return: Les metadonnées
        @rtype: str
        """
        if self._metaData:
            return self._metaData

        url = "{pigma_url}?uuid={uuid}".format(
            pigma_url=PlgOptionsManager.get_plg_settings().bm_pigma_url,
            uuid=(self._uuid if self._uuid else "8581eb13-5cb6-41d0-92a1-111e7bd7c552"),
        )
        try:
            opener = NetworkRequestsManager(False)
            reponse = opener.get_url(QUrl(url))
            if reponse is None:
                return None
            xml = minidom.parseString(reponse)

            # Xml
            strXml = xml.toprettyxml()
            if xsltTemplateFile is None:
                self._metaData = strXml
            else:
                dom = etree.fromstring(strXml)
                xslt = etree.parse(xsltTemplateFile)
                transform = etree.XSLT(xslt)
                self._metaData = str(transform(dom))

            return self._metaData
        except Exception:
            return None


class WMTSLayer(object):
    """
    Classe de gestion d'une couche WMTS
    """

    def __init__(
        self, url, key, layer, ns=PlgOptionsManager.get_plg_settings().ns_bm_uri
    ):
        """
        Constructeur
        @param url: L'url du serveur WMTS
        @type url: str
        @param key: Clé d'accés au service WMTS
        @type key: str
        @param layer: Couche WMTS du capabilities
        @type layer: LayerWMTS
        @param ns: Namespace
        @type ns: str
        """
        self._alias = None
        self._name = None
        self._format = None
        self._style = None
        self._tileMatrixSet = None
        self._wmtsMetaData = None

        self._ns = ns

        # Reading layer
        self._readLayer(layer)

        self._url = url
        self._key = key

    @property
    def alias(self):
        """
        @return: L'alias de la couche
        @rtype: str
        """
        return self._alias

    @property
    def name(self):
        """
        @return: Le nom de la couche
        @rtype: str
        """
        return self._name

    @property
    def format(self):
        """
        @return: Le format de la couche
        @rtype: str
        """
        return self._format

    @property
    def style(self):
        """
        @return: Le style de la couche
        @rtype: str
        """
        return self._style

    @property
    def tileMatrixSet(self):
        """
        @return: Le jeu de matrice de la couche
        @rtype: str
        """
        return self._tileMatrixSet

    def getMetaData(self, xsltTemplateFile=None):
        """
        Récupération des metadonnées
        @return: Les métadonnées de la couche
        @rtype: str
        """
        return self._wmtsMetaData

    def _readLayer(self, layer):
        self._name = layer.identifier
        self._alias = layer.title
        self._format = layer.format
        self._style = layer.style
        self._tileMatrixSet = layer.tileMatrixSet
        self._wmtsMetaData = layer.abstract

    def addLayer(self, showAlias):
        if self._alias not in [
            lay.name() for lay in QgsProject.instance().mapLayers().values()
        ] and self._name not in [
            lay.name() for lay in QgsProject.instance().mapLayers().values()
        ]:

            uri = QgsDataSourceUri()
            uri.setParam(
                "url",
                PlgOptionsManager.get_plg_settings().wmts_request_url
                + "?SERVICE=WMTS"
                + "&KEY="
                + PlgOptionsManager.get_plg_settings().request_key
                + "&REQUEST=GetCapabilities"
                + "&version=1.0.0",
            )
            uri.setParam("layers", self._name)
            uri.setParam(
                "crs", WMTS.instance().getTileMatrixSet(self.tileMatrixSet).supportedCRS
            )
            uri.setParam("format", self._format)
            uri.setParam("styles", self._style)
            uri.setParam("tileMatrixSet", self._tileMatrixSet)

            if PlgOptionsManager.get_plg_settings().auth_id:
                uri.setAuthConfigId(PlgOptionsManager.get_plg_settings().auth_id)

            layerTitle = self._alias if showAlias else self._name
            # let's try to add it to the map canvas
            layerWMTS = QgsRasterLayer(
                str(uri.encodedUri(), "UTF-8"), layerTitle, "wms"
            )
            if layerWMTS.isValid():
                root = QgsProject.instance().layerTreeRoot()
                groupFonds = root.findGroup("Fonds de plan")
                if not groupFonds:
                    groupFonds = root.insertGroup(len(root.children()), "Fonds de plan")
                QgsProject.instance().addMapLayer(layerWMTS, False)
                groupFonds.addLayer(layerWMTS)
            else:
                print(layerWMTS.error().message())


class WMTS(object):
    """
    Classe de base
    """

    __instance = None

    def __init__(
        self,
        url=PlgOptionsManager.get_plg_settings().wmts_request_url,
        key=PlgOptionsManager.get_plg_settings().request_key,
        ns=PlgOptionsManager.get_plg_settings().ns_bm_uri,
    ):
        """
        Constructeur
        @param url: L'url du service WMTS
        @type url: str
        @param key: La clé d'accés au service WMTS
        @type key: str
        @param ns: Namespace
        @type ns: str
        """
        self._url = url
        self._key = key
        self._layers = []
        self._tileMatrixSets = []
        self._ns = ns

        self._wmtsGetCapabilities()

    @classmethod
    def instance(cls):
        """
        Récupère l'instance du singleton
        @return: Instance du Singleton
        @rtype: WMTS
        """
        return cls.__instance

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls, *args, **kwargs)
        return cls.__instance

    def _wmtsGetCapabilities(self):
        """
        Récupère la liste des couches
        """
        try:
            wmtsClient = WMTSClient(self._url, self._key)
            wmtsClient.getCapabilities()
            for layer in wmtsClient.layers:
                # Ajout des couches
                self._layers.append(WMTSLayer(self._url, self._key, layer, ns=self._ns))
            self._tileMatrixSets = wmtsClient.tileMatrixSets
        except WMTSClientException as error:
            raise WMTSException(error)

    def getLayers(self):
        """
        Récupère la liste des couches
        @return: La liste des couches
        @rtype: list<WMTSLayer>
        """
        return self._layers

    def getLayer(self, name):
        """
        Récupère une couche par son nom
        @param name: le nom de la couche
        @type name: str
        @return: La couche
        @rtype: WMTSLayer
        """
        for layer in self._layers:
            if layer.name == name:
                return layer
        return None

    def getTileMatrixSets(self):
        """
        Récupère la liste des jeux de matrices
        @return: La liste des jeux de matrices
        @rtype: list<TileMatrixSet>
        """
        return self._tileMatrixSets

    def getTileMatrixSet(self, identifier):
        """
        Récupère un jeu de matrices par son identifiant
        @param identifier: l'identifiant du jeu de matrice
        @type identifier: str
        @return: Le jeu de matrice
        @rtype: TileMatrixSet
        """
        for tileMatrixSet in self._tileMatrixSets:
            if tileMatrixSet.identifier == identifier:
                return tileMatrixSet
        return None
