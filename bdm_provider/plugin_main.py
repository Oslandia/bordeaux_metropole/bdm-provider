#! python3  # noqa: E265

"""
    Main plugin module.
"""

import os

# standard library
from string import Template

from qgis.core import QgsApplication, QgsAuthMethodConfig
from qgis.gui import QgisInterface, QgsGui
from qgis.PyQt.QtCore import QCoreApplication

from bdm_provider.core import ProviderBdm

# PyQGIS
from bdm_provider.core.themes import ThemesDict, ThemesDictRaster

# project
from bdm_provider.gui.dlg_settings import PlgOptionsFactory
from bdm_provider.toolbelt import PlgLogger, PlgTranslator
from bdm_provider.toolbelt.preferences import PlgOptionsManager
from bdm_provider.toolbelt.wmtsclient import WMTS

# ############################################################################
# ########## Classes ###############
# ##################################


class BdmProviderPlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log

        # translation
        plg_translation_mngr = PlgTranslator(
            tpl_filename=Template("bdm_provider_$locale.qm")
        )
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr

        self.configureFromEnvVar()

    def configureFromEnvVar(self):
        # Récupération des informations d'authentification avec la variable d'environnement.
        confStr = os.getenv("BD_BMQGIS")
        if confStr and len(confStr.split(";")) == 5:
            oracleLogin = confStr.split(";")[3]
            oraclePwd = confStr.split(";")[4]

            # Création d'une configuration d'authentification
            authMgr = QgsApplication.authManager()
            if "OraConn" in authMgr.availableAuthMethodConfigs():
                authMgr.removeAuthenticationConfig("OraConn")
            p_config = QgsAuthMethodConfig()
            p_config.setName("Connection Oracle BDM")
            p_config.setMethod("Basic")
            p_config.setConfig("username", oracleLogin)
            p_config.setConfig("password", oraclePwd)
            p_config.setId("OraConn")
            if p_config.isValid():
                authMgr.storeAuthenticationConfig(p_config)

            # On configure les settings avec les données de connexion
            authId = (
                "OraConn"
                if "OraConn" in QgsApplication.authManager().configIds()
                else ""
            )
            PlgOptionsManager.set_value_from_key("oracle_auth", authId)
            PlgOptionsManager.set_value_from_key("oracle_host", confStr.split(";")[0])
            PlgOptionsManager.set_value_from_key("oracle_port", confStr.split(";")[1])
            PlgOptionsManager.set_value_from_key(
                "oracle_database", confStr.split(";")[2]
            )

    def initSingleton(self) -> bool:
        """Set up plugin singletons."""
        try:
            ThemesDictRaster()
            ThemesDict()
            WMTS()
            return True
        except Exception as error:
            self.log(
                message=(
                    "Plugin configuration error. Init ThemeDict erreur: {}".format(
                        error.args
                    )
                ),
                log_level=2,
                push=True,
            )
            return False

    def initGui(self):
        """Set up plugin UI elements."""
        # Ajout du nouveau provider
        if self.initSingleton():
            providerBdm = ProviderBdm(self.iface)
            QgsGui.sourceSelectProviderRegistry().addProvider(providerBdm)

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)
