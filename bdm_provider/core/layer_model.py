from qgis.PyQt.QtCore import QSize, QSortFilterProxyModel, Qt
from qgis.PyQt.QtGui import QIcon, QStandardItem, QStandardItemModel

from bdm_provider.__about__ import DIR_PLUGIN_ROOT
from bdm_provider.core import ThemeLayer


class LayerModel(QStandardItemModel):
    """
    Classe de données standard pour le treeview
    """

    def __init__(self, *args, **kwargs):
        super(LayerModel, self).__init__(*args, **kwargs)


class LayerProxyModel(QSortFilterProxyModel):
    """
    Classe permettant de filter l'affichage des items
    """

    def __init__(self, parent):
        self._textFilter = None
        super(LayerProxyModel, self).__init__(parent)

    def filterAcceptsRow(self, row, parent):
        """
        Filtre les lignes
        @param row: La ligne
        @type row: int
        @param parent: L'index
        @type parent: QModelIndex
        @return: Affichage de la ligne
        @rtype: bool
        """
        index = self.sourceModel().index(row, 0, parent)
        item = self.sourceModel().itemFromIndex(index)
        if isinstance(item, LayerItem) or isinstance(item, LayerWMTSItem):
            if self._textFilter is None:
                return True
            if len(self._textFilter) == 0:
                return True
            return (
                self._textFilter.lower() in item.name.lower()
                or self._textFilter.lower() in item.alias.lower()
                or item.checkState() == Qt.Checked
            )
        else:
            return True

    @property
    def textFilter(self):
        """
        @return: Le text de filtre
        @rtype: str
        """
        return self._textFilter

    @textFilter.setter
    def textFilter(self, textFilter):
        """
        @param textFilter: Le text de filtre
        @type textFilter: str
        """
        self._textFilter = textFilter


class LayerDefaultItem(QStandardItem):
    """
    Classe permettant de gérer les item standards
    """

    def __init__(self, name, alias, showAlias=True):
        """
        Constructeur
        @param name: Le nom de la couche
        @type name: str
        @param alias: L'alias de la couche
        @type alias: str
        """
        self._name = name
        self._alias = alias if alias else name
        self.childs = []

        super(LayerDefaultItem, self).__init__(self._alias if showAlias else self._name)
        self.setEditable(False)
        # finte pour éviter le double event lors d'un click
        # (celui de la checkbox et celui du click sur la ligne)
        # Donc on définit le checkable a false et on gere le setCheckState manuellement
        self.setCheckable(False)
        self.setCheckState(Qt.Unchecked)
        self.setSelectable(True)

    @property
    def name(self):
        """
        @return: Le nom
        @rtype: str
        """
        return self._name

    @property
    def alias(self):
        """
        @return: L'alias
        @rtype: str
        """
        return self._alias

    def checkedRowCount(self):
        """
        Retourne le nombre d'enfants sélectionnés
        @return: Le nombre d'enfants sélectionnés
        @rtype: int
        """
        _count = 0
        for child in self.childs:
            if child.checkState() == Qt.Checked:
                _count = _count + 1
        return _count

    def setCheckStateChild(self):
        """
        Met à jour l'état des enfants
        """
        for child in self.childs:
            child.setCheckState(self.checkState())

    def refreshCheckState(self):
        """
        Rafraichit l'état en fonction des enfants
        """
        if self.checkedRowCount() == 0:
            self.setCheckState(Qt.Unchecked)
        elif self.checkedRowCount() == self.rowCount():
            self.setCheckState(Qt.Checked)
        else:
            self.setCheckState(Qt.PartiallyChecked)

    def switchCheckState(self):
        """
        Alterne l'état
        """
        if self.checkState() == Qt.Checked:
            self.setCheckState(Qt.Unchecked)
        else:
            self.setCheckState(Qt.Checked)

    def showText(self, showAlias=True):
        """
        Permet de basculer entre alias / nom
        @param alias: Si l'affichage doit se faire par l'alias
        @type alias: bool
        """
        self.setText(self._alias if showAlias else self._name)


class LayerItemIcon(object):
    """
    Classe d'icones en fonction du type de géométrie ou de la couche
    """

    # Dictionnaire d'icones
    dictIcons = {
        ThemeLayer.GEOM_TYPE_ATTRIBUTAIRE: str(DIR_PLUGIN_ROOT)
        + "/resources/images/layer_nongraphic.png",
        ThemeLayer.GEOM_TYPE_LINEAIRE: str(DIR_PLUGIN_ROOT)
        + "/resources/images/layer_line.png",
        ThemeLayer.GEOM_TYPE_PONCTUEL: str(DIR_PLUGIN_ROOT)
        + "/resources/images/layer_point.png",
        ThemeLayer.GEOM_TYPE_SURFACIQUE: str(DIR_PLUGIN_ROOT)
        + "/resources/images/layer_polygon.png",
    }

    @staticmethod
    def getIcon(geomType):
        """
        Retourne l'icone
        @param geomType: Le type de géométrie
        @type geomType: str
        @return: L'icone
        @rtype: QIcon
        """

        icon = QIcon()
        if geomType in LayerItemIcon.dictIcons:
            resource = LayerItemIcon.dictIcons[geomType]
            icon.addFile(resource, QSize(), QIcon.Normal, QIcon.Off)
        return icon

    @staticmethod
    def getIconWMTS(layerName):
        """
        Retourne l'icone
        @param layerName: Le nom de la couche
        @type layerName: str
        @return: L'icone
        @rtype: QIcon
        """
        icon = QIcon()
        resource = str(DIR_PLUGIN_ROOT) + "/resources/images/" + layerName + ".jpg"
        icon.addFile(resource, QSize(), QIcon.Normal, QIcon.Off)
        return icon


class LayerItem(LayerDefaultItem):
    """
    Classe permettant de gérer les item de type layer
    """

    def __init__(self, layer, showAlias=True):
        """
        @param layer: La couche
        @type layer: BdmLayer
        """
        super(LayerItem, self).__init__(layer.name, layer.alias, showAlias)
        self._layer = layer

        # Icone
        if hasattr(layer, "geomType"):
            self.setIcon(LayerItemIcon.getIcon(layer.geomType))
        else:
            self.setIcon(LayerItemIcon.getIconWMTS(layer.name))

    @property
    def layer(self):
        """
        @return: La couche
        @rtype: BdmLayer
        """
        return self._layer


class LayerThemeItem(LayerDefaultItem):
    """
    Classe permettant de gérer les item de type Theme
    """

    def __init__(self, theme, showAlias=True):
        """
        @param theme: Le theme
        @type Theme: Theme
        """
        super(LayerThemeItem, self).__init__(theme.name, theme.alias, showAlias)
        self._theme = theme

        # Layer Childs
        self.childs = [LayerItem(layer, showAlias) for layer in self._theme.getLayers()]
        self.appendRows(self.childs)

    @property
    def theme(self):
        """
        @return: Le thème
        @rtype: Theme
        """
        return self._theme


class LayerWMTSItem(LayerDefaultItem):
    """
    Classe permettant de gérer les item de type wmtsLayer
    """

    def __init__(self, wmtsLayer, showAlias=True):
        """
        @param wmtsLayer: La couche WMTS
        @type wmtsLayer: WMTSLayer
        """
        super(LayerWMTSItem, self).__init__(wmtsLayer.name, wmtsLayer.alias, showAlias)
        self._wmtsLayer = wmtsLayer

        # Icone
        self.setIcon(LayerItemIcon.getIconWMTS(wmtsLayer.name))

    @property
    def wmtsLayer(self):
        """
        @return: La couche
        @rtype: WMTSLayer
        """
        return self._wmtsLayer


class LayerSpaceItem(QStandardItem):
    """
    Classe permettant de gérer les noeuds d'espace public et privé
    """

    def __init__(self, name, themes):
        """
        Constructeur
        @param name: Le nom de l'espace
        @type name: str
        @param themes: Les items des theme de l'espace
        @type themes: list<LayerThemeItem>
        """
        super(LayerSpaceItem, self).__init__(name)
        self.setEditable(False)
        self.setCheckable(False)
        self.setSelectable(False)

        self.appendRows(themes)
