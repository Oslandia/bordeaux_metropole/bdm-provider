from qgis.core import QgsProviderRegistry
from qgis.gui import QgsSourceSelectProvider
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QIcon

from bdm_provider.__about__ import DIR_PLUGIN_ROOT
from bdm_provider.core import ProviderDialog


class ProviderBdm(QgsSourceSelectProvider):
    """
    Provider pour les données de Bordeaux Métropole
    """

    def __init__(self, iface):
        super(ProviderBdm, self).__init__()

        self.iface = iface
        self.icon = QIcon(
            str(DIR_PLUGIN_ROOT) + "/resources/images/layers_selection.png"
        )
        self.name = "Données BM"
        self.ordering = 0
        self.providerKey = ""
        self.text = "Données BM"
        self.tooltip = "Données issue du Provider Bordeaux Métropole"

    def createDataSourceWidget(
        self,
        parent=None,
        fl=Qt.Widget,
        widgetMode=QgsProviderRegistry.WidgetMode.Embedded,
    ):
        return ProviderDialog(self.iface)

    def icon(self):
        return self.icon

    def name(self):
        return self.name

    def ordering(self):
        return self.ordering

    def providerKey(self):
        return self.providerKey

    def text(self):
        return self.text

    def tooltip(self):
        return self.tooltip
