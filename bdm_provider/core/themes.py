#########################################################################
#
# Copyright (C) 2016 Bordeaux Métropole
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

# pylint: disable=E0611

"""
Package de gestion des objets EP
"""
from xml.dom import minidom

from lxml import etree
from qgis.core import (
    Qgis,
    QgsDataSourceUri,
    QgsEditorWidgetSetup,
    QgsMessageLog,
    QgsProject,
    QgsRelation,
    QgsVectorLayer,
)
from qgis.PyQt.Qt import QUrl
from qgis.PyQt.QtCore import QObject

from bdm_provider.toolbelt.log_handler import PlgLogger
from bdm_provider.toolbelt.network_manager import NetworkRequestsManager
from bdm_provider.toolbelt.preferences import PlgOptionsManager
from bdm_provider.toolbelt.wmtsclient import WMTS
from bdm_provider.toolbelt.wpsclient import WPSClient, WPSClientException


class Singleton(QObject):
    """
    Singleton
    """

    __instance = None

    @classmethod
    def instance(cls):
        """
        Récupère l'instance du singleton
        """
        return cls.__instance

    @classmethod
    def clearInstance(cls):
        """
        Nettoie l'instance du singleton
        """
        cls.__instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = QObject.__new__(cls, *args, **kwargs)
        return cls.__instance


class LayerMetaData(object):
    """
    Class de gestion des métadonnées
    """

    def __init__(self, layerName):
        self.log = PlgLogger().log
        self._layerName = layerName
        self._uuid = None
        self._metaData = None

        index = self._layerName.rfind("_")
        url = (
            "{pigma_url_csw}?".format(
                pigma_url_csw=PlgOptionsManager.get_plg_settings().bm_pigma_url_csw
            )
            + "request=GetRecords&service=CSW&constraintLanguage=CQL_TEXT&version=2.0.2"
            + "&resultType=results&typeNames=csw:Record&constraint_language_version=1.1.0"
            + "&constraint=keyword like '{layerName}'".format(
                layerName=self._layerName[:index]
            )
        )
        try:
            opener = NetworkRequestsManager(False)
            reponse = opener.get_url(QUrl(url))
            if reponse:
                xml = minidom.parseString(reponse)
                elements = xml.documentElement.getElementsByTagNameNS(
                    PlgOptionsManager.get_plg_settings().ns_bm_pigma_uri, "identifier"
                )
                if len(elements) > 0 and elements[0].hasChildNodes():
                    self._uuid = elements[0].firstChild.nodeValue
        except Exception as err:
            err_msg = "Houston, we've got a problem: {}".format(err)
            self.log(message=err_msg, log_level=2, push=1)
            pass

    def getMetaData(self, xsltTemplateFile=None):
        """
        Récupération des metadonnées
        @return: Les metadonnées
        @rtype: str
        """
        if self._metaData:
            return self._metaData
        if self._uuid is None:
            return "Pas de métadonnées pour cette couche"

        url = "{pigma_url}?uuid={uuid}".format(
            pigma_url=PlgOptionsManager.get_plg_settings().bm_pigma_url, uuid=self._uuid
        )
        try:
            opener = NetworkRequestsManager(False)
            reponse = opener.get_url(QUrl(url))
            if reponse is None:
                return None
            xml = minidom.parseString(reponse)

            # Xml
            strXml = xml.toprettyxml()
            if xsltTemplateFile is None:
                self._metaData = strXml
            else:
                dom = etree.fromstring(strXml)
                xslt = etree.parse(xsltTemplateFile)
                transform = etree.XSLT(xslt)
                self._metaData = str(transform(dom))

            return self._metaData
        except Exception as err:
            err_msg = "Houston, we've got a problem: {}".format(err)
            self.log(message=err_msg, log_level=2, push=1)
            return None


class LayerRelation(object):
    """
    Classe des relations liés aux layers
    """

    def __init__(self, xml):
        """
        Constructeur
        @param xml: La chaine XML
        @type xml: str
        @param epLayer: La couche epLayer
        @type epLayer: EPLayer
        """
        self._xml = xml
        self._type = ""
        self._destination = ""
        self._destinationAlias = ""
        self._name = ""
        self._keys = []
        self._source = ""
        self._destinationLayer = None
        self._relationTable = None

        # Parsing du xml
        if self._xml:
            self._parseXml(self._xml)

    def _parseXml(self, xml):
        if xml.hasAttribute("type"):
            self._type = xml.getAttribute("type")
        if xml.hasAttribute("cible"):
            self._destination = xml.getAttribute("cible")
        if xml.hasAttribute("cibleAlias"):
            self._destinationAlias = xml.getAttribute("cibleAlias")
        if xml.hasAttribute("nom"):
            self._name = xml.getAttribute("nom")
        if xml.hasAttribute("cles"):
            self._keys = [
                cle for cle in xml.getAttribute("cles").split(",") if len(cle) > 0
            ]
        if xml.hasAttribute("source"):
            self._source = xml.getAttribute("source")


class ThemeRasterLayer(object):
    """
    Classe des layers liés aux thèmes rasters de fonds de plans
    """

    def __init__(self, themeraster, xml):
        """
        Constructeur
        @param xml: Chaine XML
        @type xml: str
        """
        self._xml = xml
        self._name = None
        self._alias = None
        self._themeraster = themeraster
        # Parsing XML
        self._parseLayer(self._xml)

    @property
    def name(self):
        """
        @return: Le nom du fond de plan
        @rtype: str
        """
        return self._name

    @property
    def alias(self):
        """
        @return: L'alias du fond de plan
        @rtype: str
        """
        return self._alias if self._alias is not None else self._name

    def _parseLayer(self, xml):
        if xml.hasAttribute("nom"):
            self._name = xml.getAttribute("nom")

        if xml.hasAttribute("alias"):
            self._alias = xml.getAttribute("alias")

    def loadLayer(self, showAlias):
        # On ne charge pas le layer si il est déjà chargé
        loadedlayers = [
            lay.name() for lay in QgsProject.instance().mapLayers().values()
        ]
        # On recupère les couches depuis le client wmts
        if self._name not in loadedlayers and self._alias not in loadedlayers:
            for wmtsLayer in WMTS.instance().getLayers():
                if wmtsLayer.name == self._name:
                    wmtsLayer.addLayer(showAlias)

    def addLayerRaster(self, showAlias):
        # On charge le fond de plan
        self.loadLayer(showAlias)


class ThemeLayer(object):
    """
    Classe des layers liés aux thèmes
    """

    GEOM_TYPE_SURFACIQUE = "SURFACIQUE"
    GEOM_TYPE_PONCTUEL = "PONCTUEL"
    GEOM_TYPE_LINEAIRE = "LINEAIRE"
    GEOM_TYPE_ATTRIBUTAIRE = "ATTRIBUTAIRE"

    def __init__(self, theme, xml):
        """
        Constructeur
        @param xml: Chaine XML
        @type xml: str
        """
        self._xml = xml
        self._theme = theme
        self._name = None
        self._alias = None
        self._geomType = None
        self._metaData = None
        self._geomFieldName = None
        self._layerId = None
        self._layerDataLoaded = False
        self._relations = []
        self._fields = []

        # Parsing XML
        self._parseLayer(self._xml)

    @property
    def name(self):
        """
        @return: Le nom de la couche
        @rtype: str
        """
        return self._name

    @property
    def alias(self):
        """
        @return: L'alias de la couche
        @rtype: str
        """
        return self._alias if self._alias is not None else self._name

    @property
    def geomType(self):
        """
        @return: Le type de géométrie de la couche
        @rtype: str
        """
        return self._geomType

    def _parseLayer(self, xml):
        if xml.hasAttribute("nom"):
            self._name = xml.getAttribute("nom")
        if xml.hasAttribute("geomType"):
            self._geomType = xml.getAttribute("geomType")
        if xml.hasAttribute("alias"):
            self._alias = xml.getAttribute("alias")

    def getMetaData(self, xsltTemplateFile=None):
        """
        Récupération des metadonnées
        @return: Les métadonnées de la couche
        @rtype: str
        """
        if self._metaData:
            return self._metaData.getMetaData(xsltTemplateFile=xsltTemplateFile)

        self._metaData = LayerMetaData(self._name)
        return self._metaData.getMetaData(xsltTemplateFile=xsltTemplateFile)

    def getLayerData(self):
        """
        Récupère les informations détaillées et les relations de la couche
        """
        wpsClient = WPSClient(
            PlgOptionsManager.get_plg_settings().request_url,
            PlgOptionsManager.get_plg_settings().request_key,
        )

        execution = wpsClient.execute("dico_couches", {"couche": self.name})
        for layer in execution.processOutputs:
            if layer.identifier == "couche":
                for data in [x for x in layer.data if x.mimeType == "text/xml"]:
                    try:
                        xml = minidom.parseString(data.data)
                    except Exception:
                        raise Exception(
                            "Erreur de parsing du xml rendu par dico_couches"
                        )

                    # Récupération des attributs et de la colonne géométrie
                    for attribut in xml.getElementsByTagNameNS(
                        PlgOptionsManager.get_plg_settings().ns_bm_uri, "Attribut"
                    ):
                        valeurs = []
                        for value in attribut.getElementsByTagNameNS(
                            PlgOptionsManager.get_plg_settings().ns_bm_uri, "Valeur"
                        ):
                            valeurs.append(
                                {
                                    "valeur": value.getAttribute("value"),
                                    "alias": value.getAttribute("alias"),
                                }
                            )

                        self._fields.append(
                            {
                                "nom": attribut.getAttribute("nom"),
                                "alias": attribut.getAttribute("alias"),
                                "valeurs": valeurs,
                            }
                        )

                        if (
                            attribut.hasAttribute("type")
                            and attribut.getAttribute("type") == "GEOMETRIE"
                        ):
                            self._geomFieldName = attribut.getAttribute("nom")

                    # Récupération des relations
                    for relation in xml.getElementsByTagNameNS(
                        PlgOptionsManager.get_plg_settings().ns_bm_uri, "Relation"
                    ):
                        self._relations.append(LayerRelation(relation))

        self._layerDataLoaded = True

    def loadLayer(self, showAlias):
        # On ne charge pas le layer si il est déjà chargé
        loadedlayers = [
            lay.name() for lay in QgsProject.instance().mapLayers().values()
        ]
        if self._name not in loadedlayers and self._alias not in loadedlayers:
            if not self._layerDataLoaded:
                self.getLayerData()

            # création de la connexion
            uri = QgsDataSourceUri()
            uri.setConnection(
                PlgOptionsManager.get_plg_settings().oracle_host,
                PlgOptionsManager.get_plg_settings().oracle_port,
                PlgOptionsManager.get_plg_settings().oracle_database,
                None,
                None,
                authConfigId=PlgOptionsManager.get_plg_settings().oracle_auth,
            )

            # Nom de la couche avec le prefix pour les données publiques
            _layerName = "{prefix}{name}".format(prefix="QG_", name=self.name)

            # Champ géométrique
            _geometryFieldName = self._geomFieldName

            # Affectation de la source
            uri.setDataSource(self._theme.name, _layerName, _geometryFieldName)

            # Attribut identifiant de la couche
            uri.setParam("key", PlgOptionsManager.get_plg_settings().oracle_id_column)

            # SRID
            uri.setSrid(PlgOptionsManager.get_plg_settings().oracle_default_srid)

            # TYPE
            if self.geomType == self.GEOM_TYPE_LINEAIRE:
                uri.setParam("type", "LineString")
            elif self.geomType == self.GEOM_TYPE_PONCTUEL:
                uri.setParam("type", "Point")
            elif self.geomType == self.GEOM_TYPE_SURFACIQUE:
                uri.setParam("type", "Polygon")

            # Ne pas utiliser les metadata
            uri.setUseEstimatedMetadata(True)

            layerOracle = QgsVectorLayer(uri.uri(False), self._name, "oracle")

            # On ajoute les valeurs des listes et les alias au besoin
            for field in layerOracle.fields():
                fieldDescription = next(
                    (att for att in self._fields if att["nom"] == field.name()), None
                )
                if fieldDescription:
                    fieldIndex = layerOracle.fields().indexOf(field.name())
                    if showAlias:
                        layerOracle.setFieldAlias(fieldIndex, fieldDescription["alias"])
                    if len(fieldDescription["valeurs"]) > 0:
                        editor_widget_setup = QgsEditorWidgetSetup(
                            "ValueMap",
                            {
                                "map": {
                                    val["alias"]: val["valeur"]
                                    for val in fieldDescription["valeurs"]
                                }
                            },
                        )
                        layerOracle.setEditorWidgetSetup(
                            fieldIndex, editor_widget_setup
                        )

            # On ajoute la couche dans le bon groupe
            root = QgsProject.instance().layerTreeRoot()

            groupLayer = root.findGroup("MetroPub")
            if not groupLayer:
                groupLayer = root.insertGroup(len(root.children()), "MetroPub")

            qgisLayer = QgsProject.instance().addMapLayer(layerOracle, False)
            groupLayer.addLayer(layerOracle)

            self._layerId = qgisLayer.id()

    def loadRelationTable(self, relation):
        # Nom de la couche avec le prefix pour les données publiques
        tableName = "{prefix}{name}".format(prefix="QG_", name=relation._name)

        # On ne charge pas la table si elle est déjà chargée
        loadedlayers = [
            lay.name() for lay in QgsProject.instance().mapLayers().values()
        ]
        if tableName not in loadedlayers:
            # création de la connexion
            uri = QgsDataSourceUri()
            uri.setConnection(
                PlgOptionsManager.get_plg_settings().oracle_host,
                PlgOptionsManager.get_plg_settings().oracle_port,
                PlgOptionsManager.get_plg_settings().oracle_database,
                None,
                None,
                authConfigId=PlgOptionsManager.get_plg_settings().oracle_auth,
            )

            # Affectation de la source
            uri.setDataSource(self._theme.name, tableName, None)

            uri.setParam("key", PlgOptionsManager.get_plg_settings().oracle_id_column)

            tableOracle = QgsVectorLayer(uri.uri(False), tableName, "oracle")

            root = QgsProject.instance().layerTreeRoot()
            groupLayer = root.findGroup("MetroPub")
            relation._relationTable = QgsProject.instance().addMapLayer(
                tableOracle, False
            )
            groupLayer.addLayer(tableOracle)
        else:
            relationTable = QgsProject.instance().mapLayersByName(tableName)[0]
            relation._relationTable = relationTable

    def addSimpleRelation(self, relation):
        rel = QgsRelation()
        rel.setReferencingLayer(relation._destinationLayer._layerId)
        rel.setReferencedLayer(self._layerId)
        if relation._source == "1":
            rel.addFieldPair(
                PlgOptionsManager.get_plg_settings().oracle_id_column, relation._keys[0]
            )
        else:
            rel.addFieldPair(
                relation._keys[0], PlgOptionsManager.get_plg_settings().oracle_id_column
            )
        rel.setName(self._name + "-" + relation._destination)
        rel.setId(self._name + "-" + relation._destination)
        if rel.isValid():
            QgsProject.instance().relationManager().addRelation(rel)

        # On ajoute le relation dans l'autre sens
        rel = QgsRelation()
        rel.setReferencingLayer(self._layerId)
        rel.setReferencedLayer(relation._destinationLayer._layerId)
        if relation._source == "1":
            rel.addFieldPair(
                relation._keys[0], PlgOptionsManager.get_plg_settings().oracle_id_column
            )
        else:
            rel.addFieldPair(
                PlgOptionsManager.get_plg_settings().oracle_id_column, relation._keys[0]
            )
        rel.setName(relation._destination + "-" + self._name)
        rel.setId(relation._destination + "-" + self._name)
        if rel.isValid():
            QgsProject.instance().relationManager().addRelation(rel)

    def addMultipleRelation(self, relation):
        # Ajout de la relation layer de reference vers table de relation
        rel = QgsRelation()
        rel.setReferencingLayer(relation._relationTable.id())
        rel.setReferencedLayer(self._layerId)
        rel.addFieldPair(
            relation._keys[0], PlgOptionsManager.get_plg_settings().oracle_id_column
        )
        rel.setName(self._name + "-RT_" + relation._destination)
        rel.setId(self._name + "-RT_" + relation._destination)
        if rel.isValid():
            QgsProject.instance().relationManager().addRelation(rel)

        # Ajout de la relation layer de destination vers table de relation
        rel = QgsRelation()
        rel.setReferencingLayer(relation._relationTable.id())
        rel.setReferencedLayer(relation._destinationLayer._layerId)
        rel.addFieldPair(
            relation._keys[1], PlgOptionsManager.get_plg_settings().oracle_id_column
        )
        rel.setName(relation._destination + "-RT_" + self._name)
        rel.setId(relation._destination + "-RT_" + self._name)
        if rel.isValid():
            QgsProject.instance().relationManager().addRelation(rel)

    def addGraphRelations(self, relation):
        # On créé deux relations vers le meme layer
        relAmont = LayerRelation(None)
        relAmont._source = "1"
        relAmont._keys = [relation._keys[0]]
        relAmont._destination = relation._destination + "_Amont"
        relAmont._destinationLayer = relation._destinationLayer
        self.addSimpleRelation(relAmont)

        relAval = LayerRelation(None)
        relAval._source = "1"
        relAval._keys = [relation._keys[1]]
        relAval._destination = relation._destination + "_Aval"
        relAval._destinationLayer = relation._destinationLayer
        self.addSimpleRelation(relAval)

    def loadRelations(self, showAlias):
        for rel in self._relations:
            rel._destinationLayer = ThemesDict.instance().getLayerByLayerName(
                rel._destination
            )
            if rel._destinationLayer:
                rel._destinationLayer.loadLayer(showAlias)
                if rel._type == "SIMPLE":
                    self.addSimpleRelation(rel)
                if rel._type == "MULTIPLE":
                    self.loadRelationTable(rel)
                    self.addMultipleRelation(rel)
                if rel._type == "GRAPHE":
                    self.addGraphRelations(rel)

    def addLayer(self, showAlias):
        # On charge la couche

        self.loadLayer(showAlias)
        layers = QgsProject.instance().mapLayers()
        monLayer = layers[self._layerId]
        # # On recharge les styles par défauts
        # # afin de recharger correctement les formulaires d'attributs
        monLayer.loadDefaultStyle()

        # Gestion des alias le cas échéant
        if showAlias:
            monLayer.setName(self._alias)

    def addLayerAndRelations(self, showAlias):
        # On charge la couche et les relations
        self.loadLayer(showAlias)
        self.loadRelations(showAlias)
        layers = QgsProject.instance().mapLayers()
        monLayer = layers[self._layerId]
        monLayer.loadDefaultStyle()
        if showAlias:
            monLayer.setName(self._alias)
            for rel in self._relations:
                destLayer = rel._destinationLayer
                layers[destLayer._layerId].setName(destLayer._alias)


class Theme(object):
    """
    Classe de gestion des thèmes
    """

    def __init__(self, xml):
        """
        Contructeur
        @param xml: Chaine XML
        @type xml: str
        """
        self._name = None
        self._alias = None
        self._layers = []
        self._xml = xml

        # Parsing XML
        self._parseTheme(xml)

    @property
    def name(self):
        """
        @return: Le nom du thème
        @rtype: str
        """
        return self._name

    @property
    def alias(self):
        """
        @return: L'alias du thème
        @rtype: str
        """
        return self._alias

    def getLayers(self):
        """
        Retourne la liste des couches du theme
        @return: La liste des couches
        @rtype: list(EPLayer)
        """
        return self._layers

    def hasLayer(self, name):
        """
        Retourne True si la couche est présente dans la liste des couches du thème
        @param name: Le nom de la couche
        @type name: str
        """
        for layer in self._layers:
            if layer.name == name:
                return True
        return False

    def getLayer(self, name):
        """
        Retourne une couche par son nom
        @param name: Le nom de la couche
        @type name: str
        @return: La couche
        @rtype: ThemeLayer
        """
        for layer in self._layers:
            if layer.name == name:
                return layer
        return None

    def _parseTheme(self, xml):
        documentElement = xml.documentElement
        if documentElement.hasAttribute("nom"):
            self._name = documentElement.getAttribute("nom")
        if documentElement.hasAttribute("alias"):
            self._alias = documentElement.getAttribute("alias")
        for child in documentElement.childNodes:
            if child.localName == "Couche":  # pour le cas de dico_themes
                self._layers.append(ThemeLayer(self, child))
            if child.localName == "GroupeCouche":  # pour le cas de dico_raster
                self._layers.append(ThemeRasterLayer(self, child))


class ThemesDictTemplate(Singleton):
    """
    Singleton de gestion des themes, utilisé pour l'appel de dico_themes pour les couches et de
    dico_raster pour les fonds de plans
    """

    def __init__(
        self,
        url=PlgOptionsManager.get_plg_settings().request_url,
        key=PlgOptionsManager.get_plg_settings().request_key,
        ns=PlgOptionsManager.get_plg_settings().ns_bm_uri,
        theme=None,
        dico_name=None,
    ):
        """
        Constructeur
        @param url: L'url du service WPS
        @type url: str
        @param key: La clé d'accés au service WPS
        @type key: str
        @param epSourcePublic: La source
        @type epSourcePublic: EPSource
        @param connectionModeler: La connexion au dictionnaire privé
        @type connectionModeler: QSqlOracleConnection
        @param ns: Namespace
        @type ns: str
        @param theme: Le nom du thème
        @type theme: str
        """
        self._url = url
        self._key = key
        self._themes = []
        self._ns = ns
        self._withAlias = PlgOptionsManager.get_plg_settings().withAlias
        self._wpsExecuteDictThemes(theme, dico_name)

    def setWithAlias(self, withAlias):
        """
        Enregistre le statut avec ou sans alias
        @param withAlias: le statut avec ou sans alias
        @type withAlias: bool
        """
        self._withAlias = withAlias
        PlgOptionsManager.set_value_from_key("withAlias", withAlias)

    def getWithAlias(self):
        """
        Récupère le statut avec ou sans alias
        @return: Le statut avec ou sans alias
        @rtype: bool
        """
        return self._withAlias

    def _wpsExecuteDictThemes(self, theme, dico_name):
        """
        Récupère la liste des themes publics
        """

        theme_identifier = "theme" if (dico_name == "dico_themes") else "groupe"
        if theme:
            inputs = {"theme": theme}
        else:
            inputs = None

        try:
            wpsClient = WPSClient(self._url, self._key)
            execution = wpsClient.execute(dico_name, inputs)
            for theme in execution.processOutputs:
                if theme.identifier == theme_identifier:
                    for data in [x for x in theme.data if x.mimeType == "text/xml"]:
                        try:
                            xml = minidom.parseString(data.data)
                        except Exception:
                            QgsMessageLog.logMessage(
                                "Erreur de parsing du xml par dico_themes",
                                "Messages",
                                Qgis.Info,
                            )
                        # Ajout des themes
                        self._themes.append(Theme(xml))
        except WPSClientException as error:
            QgsMessageLog.logMessage(
                "_wpsExecuteDictThemes erreur : {}".format(error.args),
                "Messages",
                Qgis.Info,
            )

    def getThemes(self):
        """
        Récupère la liste des themes publics
        @return: La liste des thèmes publics
        @rtype: list<EspacePublicTheme>
        """
        return self._themes

    def getTheme(self, name):
        """
        Récupère un theme public par son nom
        @param name: le nom du thème
        @type name: str
        @return: Le theme
        @rtype: EspacePublicTheme
        """
        for theme in self._themes:
            if theme.name == name:
                return theme
        return None

    def getThemeByLayerName(self, name):
        """
        Récupère le theme associé au nom d'une couche
        @param name: le nom de la couche
        @type name: str
        @return: Le nom du theme
        @rtype: str
        """
        for theme in self._themes:
            for layer in theme.getLayers():
                if layer.name == name:
                    return theme
        return None

    def getLayerByLayerName(self, name):
        """
        Récupère l'alias associé à une couche
        @param name: le nom de la couche
        @type name: str
        @return: L'alias'
        @rtype: str
        """
        for theme in self._themes:
            for layer in theme.getLayers():
                if layer.name == name:
                    return layer
        return None


class ThemesDict(ThemesDictTemplate):
    """
    On re instancie ThemesDictTemplate pour la gestion des couches
    """

    def __init__(
        self,
        url=PlgOptionsManager.get_plg_settings().request_url,
        key=PlgOptionsManager.get_plg_settings().request_key,
        ns=PlgOptionsManager.get_plg_settings().ns_bm_uri,
        theme=None,
        dico_name="dico_themes",
    ):
        """
        Constructeur
        @param url: L'url du service WPS
        @type url: str
        @param key: La clé d'accés au service WPS
        @type key: str
        @param ns: Namespace
        @type ns: str
        @param theme: Le nom du thème
        @type theme: str
        """

        super().__init__(url=url, key=key, ns=ns, theme=theme, dico_name=dico_name)


class ThemesDictRaster(ThemesDictTemplate):
    """
    On re instancie ThemesDictTemplate pour la gestion des fonds de plans
    """

    def __init__(
        self,
        url=PlgOptionsManager.get_plg_settings().request_url,
        key=PlgOptionsManager.get_plg_settings().request_key,
        ns=PlgOptionsManager.get_plg_settings().ns_bm_uri,
        theme=None,
        dico_name="dico_rasters",
    ):
        """
        Constructeur
        @param url: L'url du service WPS
        @type url: str
        @param key: La clé d'accés au service WPS
        @type key: str
        @param ns: Namespace
        @type ns: str
        @param theme: Le nom du thème
        @type theme: str
        """

        super().__init__(url=url, key=key, ns=ns, theme=theme, dico_name=dico_name)
