<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi" xmlns:geonet="http://www.fao.org/geonetwork">
	<xsl:output method="html" omit-xml-declaration="yes" />
	<xsl:template match="/">
	<div>
		<div class="panel panel-default">
			<div class="panel-heading">Général</div>
			<table class="panel-body table" border="1" cellpadding="10">
				<tr>
      				<td>Résumé</td>
					<td><xsl:value-of select="//gmd:identificationInfo//gmd:abstract/gco:CharacterString"/></td>
				</tr>
				<tr>
					<td>Mise à jour</td>
					<td>
						<xsl:choose>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'annually'">Annuelle</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'biannually'">Bi-annuelle</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'fortnightly'">Bi-mensuelle</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'continual'">Continue</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'weekly'">Hebdomadaire</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'unknown'">Inconnue</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'irregular'">Irrégulière</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'daily'">Journalière</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'asNeeded'">Lorsque nécessaire</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'monthly'">Mensuelle</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'notPlanned'">Non planifiée</xsl:when>
							<xsl:when test="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue = 'quarterly'">Trimestrielle</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="//gmd:resourceMaintenance//gmd:MD_MaintenanceFrequencyCode/@codeListValue" />
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>

				<!-- Dates -->
				<xsl:for-each select="//gmd:identificationInfo//gmd:citation/gmd:CI_Citation/gmd:date">
					<tr>
						<xsl:choose>
							<xsl:when test=".//gmd:dateType/gmd:CI_DateTypeCode/@codetdstValue = 'creation'">
								<td>Date de création</td>
								<td>
									<xsl:value-of select="./gmd:CI_Date/gmd:date/gco:Date"/>
								</td>
							</xsl:when>
							<xsl:when test=".//gmd:dateType/gmd:CI_DateTypeCode/@codetdstValue = 'revision'">
								<td>Date de révision</td>
								<td>
									<xsl:value-of select="./gmd:CI_Date/gmd:date/gco:Date"/>
								</td>
							</xsl:when>
							<xsl:when test=".//gmd:dateType/gmd:CI_DateTypeCode/@codetdstValue = 'vatddity'">
								<td>Date de vatddité</td>
								<td>
									<xsl:value-of select="./gmd:CI_Date/gmd:date/gco:Date"/>
								</td>
							</xsl:when>
							<xsl:when test=".//gmd:dateType/gmd:CI_DateTypeCode/@codetdstValue = 'pubtdcation'">
								<td>Date de pubtdcation</td>
								<td>
									<xsl:value-of select="./gmd:CI_Date/gmd:date/gco:Date"/>
								</td>
							</xsl:when>
						</xsl:choose>
					</tr>
				</xsl:for-each>

				<xsl:if test="//gmd:resourceMaintenance//gmd:maintenanceNote/gco:CharacterString != ''">
				<tr>
					<td>Remarque sur la mise à jour</td>
					<td><xsl:value-of select="//gmd:resourceMaintenance//gmd:maintenanceNote/gco:CharacterString"/></td>
				</tr>
				</xsl:if>
			</table>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">Généalogie</div>
			<table class="panel-body table" border="1" cellpadding="10">
				<tr>
					<td>Objectifs</td>
					<td><xsl:value-of select="//gmd:identificationInfo//gmd:purpose/gco:CharacterString"/></td>
				</tr>
				<tr>
					<td>Origine des données</td>
					<td><xsl:value-of select="//gmd:lineage//gmd:statement/gco:CharacterString"/></td>
				</tr>
			</table>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">Contraintes légales</div>
			<table class="panel-body table" border="1" cellpadding="10">
				<xsl:for-each select="//gmd:resourceConstraints//gmd:useLimitation">
				<tr>
					<td>Limite d'utilisation</td>
					<td><xsl:value-of select="./gco:CharacterString"/></td>
				</tr>
				</xsl:for-each>
				<tr>
					<td>Autres contraintes</td>
					<td><xsl:value-of select="//gmd:resourceConstraints//gmd:otherConstraints//gco:CharacterString"/></td>
				</tr>
			</table>
		</div>

		<!--<xsl:element name="a">
		<xsl:attribute name="href">http://metadata.bordeaux-metropole.fr/geosource/apps/search/?uuid=
		<xsl:value-of select="//gmd:fileIdentifier/gco:CharacterString"/></xsl:attribute>
		<xsl:attribute name="target">_blank</xsl:attribute>
		Accéder à la fiche de métadonnées complète
		</xsl:element>-->
	</div>
	</xsl:template>
</xsl:stylesheet>
