# Develoment dependencies
# -----------------------

black
flake8>=3.8,<3.10
flake8-builtins>=1.5,<1.6
flake8-isort>=4.0,<4.1
isort>=5.8,<5.10
pre-commit>=2.12,<2.14
